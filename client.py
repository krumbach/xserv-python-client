import json
import datetime
import calendar

from requests.sessions import Session
from requests.auth import HTTPBasicAuth
from urlparse import urljoin

class XservClient(Session):

    base_url = "https://www.xserv.us/api"

    def __init__(self, api_key):
        super(XservClient, self).__init__()

        self.auth = HTTPBasicAuth(api_key, 'not used')
        self.headers['Content-Type'] = 'application/json'
        self._json_encoder = CustomModelEncoder()
        self.verify = False

    def request(self, method, url, *args, **kwargs):

        join_url = urljoin(self.base_url, url)

        if 'data' in kwargs:
            json_data = self._encode_data(kwargs['data'])
            kwargs['data'] = json_data

        return super(XservClient, self).request(method, join_url, *args, **kwargs)

    def _encode_data(self, data, **kwargs):
        encoder = CustomModelEncoder(**kwargs) if kwargs else self._json_encoder
        return encoder.encode(data)


class CustomModelEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return calendar.timegm(obj.utctimetuple())
        elif hasattr(obj, '__data__'):
            return obj.__data__()
        else:
            return json.JSONEncoder.default(self, obj)