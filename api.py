from client import XservClient
from proxies import TrendsProxy
from proxies import LocationsProxy


class XservApi(object):

    api_version = 'v1'

    def __init__(self, api_key):
        self.client = XservClient(api_key)
        self.client.base_url += '/{}/'.format(self.api_version)
        self._trends = TrendsProxy(self.client)
        self._locations = LocationsProxy(self.client)

    @property
    def trends(self):
        return self._trends

    @property
    def locations(self):
        return self._locations





