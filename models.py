class Base(object):
    def __init__(self):
        self._data = {}

    def __getattr__(self, name):
        try:
            return self._data[name]
        except KeyError:
            class_name = self.__class__.__name__
            raise AttributeError(
                "'{}' object has no attribute '{}'".format(class_name, name))

    def __setattr__(self, name, value):
        if not name.startswith('_') and name not in dir(self.__class__):
            self._data[name] = value
        else:
            super(Base, self).__setattr__(name, value)

    def __data__(self):
        # remove all none elements
        return {k: v for k, v in self._data.items() if v is not None}


class Trend(Base):
    def __init__(self, name, value, timestamp):
        super(Trend, self).__init__()

        self._data = {
            'name': name,
            'value': value,
            'timestamp': timestamp
        }

class Location(Base):
    def __init__(self, name, lat, lng, timestamp):
        super(Location, self).__init__()

        self._data = {
            'name': name,
            'lat': lat,
            'lng': lng,
            'timestamp': timestamp
        }
