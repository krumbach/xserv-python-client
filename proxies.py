from models import Trend
from models import Location


class TrendsProxy(object):
    model_path = 'trends/decimal'

    def __init__(self, client):
        self.url = client.base_url + self.model_path
        self.client = client

    def create(self, name, value, timestamp):
        trend = Trend(name, value, timestamp)
        resp = self.client.post(self.url, data=trend)
        resp.raise_for_status()

        data = resp.json()

        return data


class LocationsProxy(object):
    model_path = 'trends/gps'

    def __init__(self, client):
        self.url = client.base_url + self.model_path
        self.client = None

    def create(self, name, lat, lng, timestamp):
        loc = Location(name, lat, lng, timestamp)
        resp = self.client.post(self.url, data=loc)
        resp.raise_for_status()

        data = resp.json()

        return data.pop('data')









